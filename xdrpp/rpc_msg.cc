
#include <iostream>
#include <xdrpp/exception.h>

namespace xdr {

using std::string;

const char *
rpc_errmsg(accept_stat ev)
{
  switch(ev) {
  case accept_stat::SUCCESS:
    return "RPC executed successfully";
  case accept_stat::PROG_UNAVAIL:
    return "remote hasn't exported program";
  case accept_stat::PROG_MISMATCH:
    return "remote can't support version #";
  case accept_stat::PROC_UNAVAIL:
    return "program can't support procedure";
  case accept_stat::GARBAGE_ARGS:
    return "procedure can't decode params";
  case accept_stat::SYSTEM_ERR:
    return "RPC system error";
  default:
    return "unknown accept_stat error";
  }
}

const char *
rpc_errmsg(auth_stat ev)
{
  switch(ev) {
  case auth_stat::AUTH_OK:
    return "success";
  case auth_stat::AUTH_BADCRED:
    return "bad credential (seal broken)";
  case auth_stat::AUTH_REJECTEDCRED:
    return "client must begin new session";
  case auth_stat::AUTH_BADVERF:
    return "bad verifier (seal broken)";
  case auth_stat::AUTH_REJECTEDVERF:
    return "verifier expired or replayed";
  case auth_stat::AUTH_TOOWEAK:
    return "rejected for security reasons";
  case auth_stat::AUTH_INVALIDRESP:
    return "bogus response verifier";
  case auth_stat::AUTH_FAILED:
    return "reason unknown";
  case auth_stat::AUTH_KERB_GENERIC:
    return "kerberos generic error";
  case auth_stat::AUTH_TIMEEXPIRE:
    return "time of credential expired";
  case auth_stat::AUTH_TKT_FILE:
    return "problem with ticket file";
  case auth_stat::AUTH_DECODE:
    return "can't decode authenticator";
  case auth_stat::AUTH_NET_ADDR:
    return "wrong net address in ticket";
  case auth_stat::RPCSEC_GSS_CREDPROBLEM:
    return "no credentials for user";
  case auth_stat::RPCSEC_GSS_CTXPROBLEM:
    return "problem with context";
  default:
    return "auth_stat error";
  }
}

rpc_call_stat::rpc_call_stat(const rpc_msg &hdr)
{
  if (hdr.body.mtype() == msg_type::REPLY)
    switch (hdr.body.rbody().stat()) {
    case reply_stat::MSG_ACCEPTED:
      type_ = ACCEPT_STAT;
      accept_ = hdr.body.rbody().areply().reply_data.stat();
      return;
    case reply_stat::MSG_DENIED:
      accept_ = accept_stat::SUCCESS;
      switch (hdr.body.rbody().rreply().stat()) {
      case reject_stat::RPC_MISMATCH:
	    type_ = RPCVERS_MISMATCH;
	    return;
      case reject_stat::AUTH_ERROR:
	    type_ = AUTH_STAT;
	    auth_ = hdr.body.rbody().rreply().rj_why();
	    return;
      }
    }
  type_ = GARBAGE_RES;
}

const char *
rpc_call_stat::message() const
{
  switch (type_) {
  case ACCEPT_STAT:
    return rpc_errmsg(accept_);
  case AUTH_STAT:
    return rpc_errmsg(auth_);
  case RPCVERS_MISMATCH:
    return "server reported rpcvers field with wrong value";
  case GARBAGE_RES:
    return "unable to unmarshal reply value sent by server";
  case NETWORK_ERROR:
    return "network error when communicating with server";
  case BAD_ALLOC:
    return "insufficient memory to unmarshal result";
  default:
    std::cerr << "rpc_call_stat: invalid type" << std::endl;
    std::terminate();
  }
}

void
check_call_hdr(const rpc_msg &hdr)
{
  if (hdr.body.mtype() != msg_type::REPLY)
    throw xdr_runtime_error("call received when reply expected");
  switch (hdr.body.rbody().stat()) {
    case reply_stat::MSG_ACCEPTED:
    if (hdr.body.rbody().areply().reply_data.stat() == accept_stat::SUCCESS)
      return;
    throw xdr_call_error(hdr.body.rbody().areply().reply_data.stat());
    case reply_stat::MSG_DENIED:
    if (hdr.body.rbody().rreply().stat() == reject_stat::AUTH_ERROR)
      throw xdr_call_error(hdr.body.rbody().rreply().rj_why());
    throw xdr_call_error(rpc_call_stat::RPCVERS_MISMATCH);
  default:
    throw xdr_runtime_error("check_call_hdr: garbage reply_stat");
  }
}

}
